//
//
//
export namespace Type {
    /**
     * @example
     * typeof value === 'boolean'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isBoolean(value: unknown): value is boolean {
        return typeof value === 'boolean';
    }
    /**
     * @example
     * typeof value !== 'boolean'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotBoolean<Type>(value: Type): value is Exclude<Type, boolean> {
        return typeof value !== 'boolean';
    }

    /**
     * @example
     * value === true
     */
    export function isTrue(value: unknown): boolean {
        return value === true;
    }
    /**
     * @example
     * value !== true
     */
    export function isNotTrue(value: unknown): boolean {
        return value !== true;
    }

    /**
     * @example
     * value === false
     */
    export function isFalse(value: unknown): boolean {
        return value === false;
    }
    /**
     * @example
     * value !== false
     */
    export function isNotFalse(value: unknown): boolean {
        return value !== false;
    }

    /**
     * @example
     * !!value
     *
     * @see https://developer.mozilla.org/docs/Glossary/truthy
     */
    export function isTruthy(value: unknown): boolean {
        return !!value;
    }
    /**
     * @example
     * !value
     *
     * @see https://developer.mozilla.org/docs/Glossary/falsy
     */
    export function isFalsy(value: unknown): boolean {
        return !value;
    }

    /**
     * @example
     * typeof value === 'function'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isFunction(value: unknown): value is Function {
        return typeof value === 'function';
    }
    /**
     * @example
     * typeof value !== 'function'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotFunction<Type>(value: Type): value is Exclude<Type, Function> {
        return typeof value !== 'function';
    }

    /**
     * @example
     * typeof value === 'number'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNumber(value: unknown): value is number {
        return typeof value === 'number';
    }
    /**
     * @example
     * typeof value !== 'number'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotNumber<Type>(value: Type): value is Exclude<Type, number> {
        return typeof value !== 'number';
    }

    /**
     * @example
     * Number.isNaN(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
     */
    export function isNaN(value: number): boolean {
        return Number.isNaN(value);
    }
    /**
     * @example
     * !isNaN(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isNaN
     */
    export function isNotNaN(value: number): boolean {
        return !isNaN(value);
    }

    /**
     * @example
     * Number.isInteger(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
     */
    export function isInteger(value: number): boolean {
        return Number.isInteger(value);
    }
    /**
     * @example
     * !isInteger(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger
     */
    export function isNotInteger(value: number): boolean {
        return !isInteger(value);
    }

    /**
     * @example
     * Number.isSafeInteger(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger
     */
    export function isSafeInteger(value: number): boolean {
        return Number.isSafeInteger(value);
    }
    /**
     * @example
     * !isSafeInteger(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isSafeInteger
     */
    export function isNotSafeInteger(value: number): boolean {
        return !isSafeInteger(value);
    }

    /**
     * @example
     * Number.isFinite(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite
     */
    export function isFinite(value: number): boolean {
        return Number.isFinite(value);
    }
    /**
     * @example
     * !isFinite(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number/isFinite
     */
    export function isInfinite(value: number): boolean {
        return !isFinite(value);
    }

    /**
     * @example
     * isFinite(value) && isNotInteger(value)
     */
    export function isFloat(value: number): boolean {
        return isFinite(value) && isNotInteger(value);
    }
    /**
     * @example
     * !isFloat(value)
     */
    export function isNotFloat(value: number): boolean {
        return !isFloat(value);
    }

    /**
     * @example
     * value === 0
     */
    export function isZero(value: number): boolean {
        return value === 0;
    }
    /**
     * @example
     * value !== 0
     */
    export function isNotZero(value: number): boolean {
        return value !== 0;
    }

    /**
     * @example
     * value > 0
     */
    export function isPositiveNumber(value: number): boolean {
        return value > 0;
    }
    /**
     * @example
     * value <= 0
     */
    export function isNotPositiveNumber(value: number): boolean {
        return value <= 0;
    }

    /**
     * @example
     * value < 0
     */
    export function isNegativeNumber(value: number): boolean {
        return value < 0;
    }
    /**
     * @example
     * value >= 0
     */
    export function isNotNegativeNumber(value: number): boolean {
        return value >= 0;
    }

    /**
     * @example
     * typeof value === 'object' && isNotNull(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isObject(value: unknown): value is object {
        return typeof value === 'object' && isNotNull(value);
    }
    /**
     * @example
     * !isObject(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotObject<Type>(value: Type): value is Exclude<Type, object> {
        return !isObject(value);
    }

    /**
     * @example
     * Object.isExtensible(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible
     */
    export function isExtensible(value: object): boolean {
        return Object.isExtensible(value);
    }
    /**
     * @example
     * !isExtensible(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/isExtensible
     */
    export function isNotExtensible(value: object): boolean {
        return !isExtensible(value);
    }

    /**
     * @example
     * Object.isFrozen(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/isFrozen
     */
    export function isFrozen(value: object): boolean {
        return Object.isFrozen(value);
    }
    /**
     * @example
     * !isFrozen(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/isFrozen
     */
    export function isNotFrozen(value: object): boolean {
        return !isFrozen(value);
    }

    /**
     * @example
     * Object.isSealed(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/isSealed
     */
    export function isSealed(value: object): boolean {
        return Object.isSealed(value);
    }
    /**
     * @example
     * !isSealed(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object/isSealed
     */
    export function isNotSealed(value: object): boolean {
        return !isSealed(value);
    }

    /**
     * @example
     * typeof value === 'string'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isString(value: unknown): value is string {
        return typeof value === 'string';
    }
    /**
     * @example
     * typeof value === 'string'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotString<Type>(value: Type): value is Exclude<Type, string> {
        return typeof value !== 'string';
    }

    /**
     * @example
     * value === ''
     */
    export function isEmptyString(value: unknown): value is '' {
        return value === '';
    }
    /**
     * @example
     * !isEmptyString(value)
     */
    export function isNotEmptyString<Type>(value: Type): value is Exclude<Type, ''> {
        return !isEmptyString(value);
    }

    /**
     * @example
     * typeof value === 'symbol'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isSymbol(value: unknown): value is symbol {
        return typeof value === 'symbol';
    }
    /**
     * @example
     * typeof value !== 'symbol'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotSymbol<Type>(value: Type): value is Exclude<Type, symbol> {
        return typeof value !== 'symbol';
    }

    /**
     * @example
     * typeof value === 'undefined'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isUndefined(value: unknown): value is undefined {
        return typeof value === 'undefined';
    }
    /**
     * @example
     * typeof value !== 'undefined'
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/typeof
     */
    export function isNotUndefined<Type>(value: Type): value is Exclude<Type, undefined> {
        return typeof value !== 'undefined';
    }

    /**
     * @example
     * value === null
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/null
     */
    export function isNull(value: unknown): value is null {
        return value === null;
    }
    /**
     * @example
     * value !== null
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/null
     */
    export function isNotNull<Type>(value: Type): value is Exclude<Type, null> {
        return value !== null;
    }

    /**
     * @example
     * isUndefined(value) || isNull(value)
     */
    export function isUndefinedOrNull(value: unknown): value is undefined | null {
        return isUndefined(value) || isNull(value);
    }
    /**
     * @example
     * !isUndefinedOrNull(value)
     */
    export function isNotUndefinedAndNotNull<Type>(value: Type): value is Exclude<Type, undefined | null> {
        return !isUndefinedOrNull(value);
    }

    /**
     * @example
     * Array.isArray(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
     */
    export function isArray<Item, ArrayType extends Array<Item> | ReadonlyArray<Item>>(value: unknown): value is ArrayType {
        return Array.isArray(value);
    }
    /**
     * @example
     * !isArray(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Array/isArray
     */
    export function isNotArray<Type>(value: Type): value is Exclude<Type, Array<unknown> | ReadonlyArray<unknown>> {
        return !isArray(value);
    }

    /**
     * @example
     * value instanceof Date
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isDate(value: unknown): value is Date {
        return value instanceof Date;
    }
    /**
     * @example
     * !isDate(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isNotDate<Type>(value: Type): value is Exclude<Type, Date> {
        return !isDate(value);
    }

    /**
     * @example
     * value instanceof Promise
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isPromise<Result>(value: unknown): value is Promise<Result> {
        return value instanceof Promise;
    }
    /**
     * @example
     * !isPromise(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isNotPromise<Type>(value: Type): value is Exclude<Type, Promise<unknown>> {
        return !isPromise(value);
    }

    /**
     * @example
     * value instanceof RegExp
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isRegExp(value: unknown): value is RegExp {
        return value instanceof RegExp;
    }
    /**
     * @example
     * !isRegExp(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isNotRegExp<Type>(value: Type): value is Exclude<Type, RegExp> {
        return !isRegExp(value);
    }

    /**
     * @example
     * value instanceof Error
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Error
     */
    export function isError(value: unknown): value is Error {
        return value instanceof Error;
    }
    /**
     * @example
     * !isError(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Error
     */
    export function isNotError<Type>(value: Type): value is Exclude<Type, Error> {
        return !isError(value);
    }

    /**
     * @example
     * value instanceof EvalError
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/EvalError
     */
    export function isEvalError(value: unknown): value is EvalError {
        return value instanceof EvalError;
    }
    /**
     * @example
     * !isEvalError(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/EvalError
     */
    export function isNotEvalError<Type>(value: Type): value is Exclude<Type, EvalError> {
        return !isEvalError(value);
    }

    /**
     * @example
     * value instanceof RangeError
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/RangeError
     */
    export function isRangeError(value: unknown): value is RangeError {
        return value instanceof RangeError;
    }
    /**
     * @example
     * !isRangeError(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/RangeError
     */
    export function isNotRangeError<Type>(value: Type): value is Exclude<Type, RangeError> {
        return !isRangeError(value);
    }

    /**
     * @example
     * value instanceof ReferenceError
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError
     */
    export function isReferenceError(value: unknown): value is ReferenceError {
        return value instanceof ReferenceError;
    }
    /**
     * @example
     * !isReferenceError(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError
     */
    export function isNotReferenceError<Type>(value: Type): value is Exclude<Type, ReferenceError> {
        return !isReferenceError(value);
    }

    /**
     * @example
     * value instanceof SyntaxError
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError
     */
    export function isSyntaxError(value: unknown): value is SyntaxError {
        return value instanceof SyntaxError;
    }
    /**
     * @example
     * !isSyntaxError(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError
     */
    export function isNotSyntaxError<Type>(value: Type): value is Exclude<Type, SyntaxError> {
        return !isSyntaxError(value);
    }

    /**
     * @example
     * value instanceof TypeError
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/TypeError
     */
    export function isTypeError(value: unknown): value is TypeError {
        return value instanceof TypeError;
    }
    /**
     * @example
     * !isTypeError(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/TypeError
     */
    export function isNotTypeError<Type>(value: Type): value is Exclude<Type, TypeError> {
        return !isTypeError(value);
    }

    /**
     * @example
     * value instanceof Proxy
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isProxy<Target extends object>(value: unknown): value is ProxyHandler<Target> {
        return value instanceof Proxy;
    }
    /**
     * @example
     * !isProxy(value)
     *
     * @see https://developer.mozilla.org/docs/Web/JavaScript/Reference/Operators/instanceof
     */
    export function isNotProxy<Type>(value: Type): value is Exclude<Type, ProxyHandler<object>> {
        return !isProxy(value);
    }

    /**
     * @example
     * isUndefinedOrNull(value) || isEmptyString(value)
     */
    export function isEmpty(value: unknown): value is undefined | null | '' {
        return isUndefinedOrNull(value) || isEmptyString(value);
    }
    /**
     * @example
     * !isEmpty(value)
     */
    export function isNotEmpty<Type>(value: Type): value is Exclude<Type, undefined | null | ''> {
        return !isEmpty(value);
    }
}
