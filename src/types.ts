//
//
//
export type Index<Keys extends string | number | symbol, Value> = { [Key in Keys]: Value; };
export type ReadonlyIndex<Keys extends string | number | symbol, Value> = Readonly<Index<Keys, Value>>;

export type Nullable<Type> = Type | undefined | null;

export type Writable<Type extends object> = {
    -readonly [Key in keyof Type]: Type[Key];
};

export type Newable<Type> = new (...parameters: Array<any>) => Type;

export type Callable<Parameters extends Array<any> = [], Result = void> = (...parameters: Parameters) => Result;
