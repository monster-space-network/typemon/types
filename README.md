# Types - [![npm-version](https://img.shields.io/npm/v/@typemon/types.svg)](https://www.npmjs.com/package/@typemon/types) [![npm-downloads](https://img.shields.io/npm/dt/@typemon/types.svg)](https://www.npmjs.com/package/@typemon/types)
> Useful types, interfaces and utilities



## About
We wanted to use the type more powerfully.
So we needed more types and a powerful type checker and eventually created it.
I hope this helps.



## Features
- `Types, Interfaces`
    - `Index`
    - `ReadonlyIndex`
    - `Nullable`
    - `Writable`
    - `Newable`
    - `Callable`
+ `Type`
    - `isBoolean`
    - `isNotBoolean`
    - `isTrue`
    - `isNotTrue`
    - `isFalse`
    - `isNotFalse`
    - `isTruthy`
    - `isFalsy`
    - `isFunction`
    - `isNotFunction`
    - `isNumber`
    - `isNotNumber`
    - `isNaN`
    - `isNotNaN`
    - `isInteger`
    - `isNotInteger`
    - `isSafeInteger`
    - `isNotSafeInteger`
    - `isFinite`
    - `isInfinite`
    - `isFloat`
    - `isNotFloat`
    - `isZero`
    - `isNotZero`
    - `isPositiveNumber`
    - `isNotPositiveNumber`
    - `isNegativeNumber`
    - `isNotNegativeNumber`
    - `isObject`
    - `isNotObject`
    - `isExtensible`
    - `isNotExtensible`
    - `isFrozen`
    - `isNotFrozen`
    - `isSealed`
    - `isNotSealed`
    - `isString`
    - `isNotString`
    - `isEmptyString`
    - `isNotEmptyString`
    - `isSymbol`
    - `isNotSymbol`
    - `isUndefined`
    - `isNotUndefined`
    - `isNull`
    - `isNotNull`
    - `isUndefinedOrNull`
    - `isNotUndefinedAndNotNull`
    - `isArray`
    - `isNotArray`
    - `isDate`
    - `isNotDate`
    - `isPromise`
    - `isNotPromise`
    - `isRegExp`
    - `isNotRegExp`
    - `isError`
    - `isNotError`
    - `isEvalError`
    - `isNotEvalError`
    - `isRangeError`
    - `isNotRangeError`
    - `isReferenceError`
    - `isNotReferenceError`
    - `isSyntaxError`
    - `isNotSyntaxError`
    - `isTypeError`
    - `isNotTypeError`
    - `isProxy`
    - `isNotProxy`
    - `isEmpty`
    - `isNotEmpty`



## Installation
```
$ npm install @typemon/types
```
```typescript
import {
    /* Types, Interfaces */
    Index, ReadonlyIndex,
    Nullable,
    Writable,
    Newable,
    Callable,

    /* Type checker */
    Type
} from '@typemon/types';
```
